const tasks = [
  { title: "Faire les courses", isComplete: false },
  { title: "Nettoyer la maison", isComplete: true },
  { title: "Planter le jardin", isComplete: false },
];

// Créez une fonction addTask, en utilisant une fonction fléchée et l'opérateur spread.

const addTask = (task) => {
  let newTask = [...task, ...tasks];
  return newTask;
};

const taskOne = [{ title: "Ranger le placard", isComplete: false }];

console.log(addTask(taskOne));

// Fonction fléchée qui récupère les tâches du tableau "tasks" et renvoie seulement les tâches non complétées, ne fonctionne que sur le tableau tasks
const incompleteTasks = tasks.filter((eachtask) => {
  if (eachtask.isComplete === false) {
    return true;
  } else {
    return false;
  }
});

console.log(incompleteTasks);

// Créez une fonction fléchée `toggleTaskStatus` qui prend en paramètres une tâche.

// Retournez une nouvelle tâche contenant toutes ses informations, et dont le statut "terminé" de la tâche a été inversé (true -> false, false -> true).

// Vous avez la possibilité d'utiliser le spread operator (mais ce n'est pas obligatoire).

const taskTwo = { title: "Faire la vaisselle", isComplete: false };

const toggleTaskStatus = (singleTask) => {
  if (singleTask.isComplete === false) {
    singleTask.isComplete = true;
  } else if (singleTask.isComplete === true) {
    singleTask.isComplete = false;
  }
  return singleTask;
};

console.log(toggleTaskStatus(taskTwo));
